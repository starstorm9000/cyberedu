package applications;
// sample ball simulator to show the capability of DesktopEnvironment and
// the game engine
// feel free to use as an example for how to do drawing, user input, etc.

import java.awt.Graphics;
import java.awt.Color;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.util.ArrayList;

public class BouncingBall extends main.WindowedApplication implements MouseListener, MouseMotionListener, MouseWheelListener {

	private static final long serialVersionUID = 1L;
	private ArrayList<Ball> balls = new ArrayList<Ball>();
	private int posX, posY, initialX, initialY, radius = 20;
	private boolean dragging = false;
	private boolean spirit = false;
	public BouncingBall() {
		width = height = 200;
		addMouseListener(this);
		addMouseMotionListener(this);
		addMouseWheelListener(this);
	}
	
	public void update() {
		for (Ball b : balls) {
			// updates all the balls (increments their position based on their
			// velocity)
			b.update(width, height);
		}
		for (int i = 0; i < balls.size(); i ++) {
			for (int j = 0; j < balls.size(); j ++) {
				if (i != j) {
					// check each ball for a collision with any other ball
					balls.get(i).simulateCollision(balls.get(j));
				}
			}
		}
	}
	
	@Override
	public void paintComponent(Graphics g) {
		Toolkit.getDefaultToolkit().sync();
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, width, height);
		g.setColor(Color.RED);
		if (dragging) {
			g.drawLine(posX, posY, initialX, initialY);
		}
		for (Ball s : balls) {
			for (Ball b : balls) {
				s.castShadow(g, b);
			}
		}
		for (Ball b : balls) {
			b.paint(g);
		}
		if (!spirit) {
			g.setColor(new Color(100, 100, 100, 127));
		} else {
			g.setColor(new Color(238, 238, 0, 127));
		}
		// if the mouse is being dragged, the translucent circle is drawn where it was pressed down
		g.fillOval((dragging? initialX : posX) - radius, (dragging? initialY : posY) - radius, 2 * radius, 2 * radius);
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		posX = e.getX();
		posY = e.getY();
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		posX = e.getX();
		posY = e.getY();
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// if the mouse was right clicked, light ball mode toggled
		if (e.getButton() == 3) {
			spirit = !spirit;
		}
	}

	@Override
	public void mouseEntered(MouseEvent e) {}

	@Override
	public void mouseExited(MouseEvent e) {}

	@Override
	public void mousePressed(MouseEvent e) {
		if (e.getButton() == MouseEvent.BUTTON1) {
			dragging = true;
			initialX = e.getX();
			initialY = e.getY();
		} else if (e.getButton() == MouseEvent.BUTTON3) {
			for (Ball b : balls) {
				if (b.collides(posX,  posY)) {
					b.setColor(Color.RED);
				}
			}
		}
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		if (dragging) {
			dragging = false;
			if (!spirit) {
				balls.add(new Ball(initialX, initialY, (initialX - posX) / 4, (initialY - posY) / 4, radius));
			} else {
				balls.add(new Spirit(initialX, initialY, (initialX - posX) / 4, (initialY - posY) / 4, radius));
			}
		}
	}

	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {
		// changes the radius of the next ball to be placed
		radius -= e.getWheelRotation();
		if (radius < 1) {
			radius = 1;
		}
	}

	private class Ball {

		protected double px, py, vx, vy, radius, m;
		protected Color color;

		public Ball(int x, int y, int vx, int vy, int r) {
			this.m = 1;
			this.px = x;
			this.py = y;
			this.vx = vx;
			this.vy = vy;
			this.radius = r;
			color = new Color((int) (Math.random() * 255), 
					(int) (Math.random() * 255), 
					(int) (Math.random() * 255));
		}

		// increments the position of the ball, checking to ensure within bounds
		public void update(int width, int height) {
			px += vx;
			py += vy;
			if (px - radius < 0) {
				vx = -vx;
				px = radius;
			}
			if (px + radius > width) {
				vx = -vx;
				px = width - radius;
			}
			if (py - radius < 0) {
				vy = -vy;
				py = radius;
			}
			if (py + radius > height) {
				vy = -vy;
				py = height - radius;
			}
		}

		public void setColor(Color c) {
			color = c;
		}

		public double getX() {
			return px;
		}

		public double getY() {
			return py;
		}

		public void setX(double x) {
			px = x;
		}

		public void setY(double y) {
			py = y;
		}

		public double getVX() {
			return vx;
		}

		public double getVY() {
			return vy;
		}

		public void setVX(double d) {
			vx = d;
		}

		public void setVY(double d) {
			vy = d;
		}

		public double[] getPosition() {
			return new double[] {px, py};
		}

		public double getMass() {
			return m;
		}

		public double getRadius() {
			return radius;
		}

		// calculates the angle a ball should move in based on how it collided
		// with another ball
		// this math isn't accurate, but it looks good enough
		public void simulateCollision(Ball b) {
			if (collides(b)) {
				double midpointx = (px + b.getX()) / 2; 
				double midpointy = (py + b.getY()) / 2;
				//				
				px = midpointx + radius * (px - b.getX()) / Math.sqrt(Math.pow(px - b.getX(), 2) + Math.pow(py - b.getY(), 2)); 
				py = midpointy + radius * (py - b.getY()) / Math.sqrt(Math.pow(px - b.getX(), 2) + Math.pow(py - b.getY(), 2)); 
				b.setX(midpointx + b.getRadius() * (b.getX() - px) / Math.sqrt(Math.pow(px - b.getX(), 2) + Math.pow(py - b.getY(), 2))); 
				b.setY(midpointy + b.getRadius() * (b.getY() - py) / Math.sqrt(Math.pow(px - b.getX(), 2) + Math.pow(py - b.getY(), 2)));



				double nvx1 = (vx * (m - b.getMass()) + (2 * b.getMass() * b.getVX())) / (m + b.getMass());
				double nvy1 = (b.getVX() * (b.getMass() - m) + (2 * m * vx)) / (m + b.getMass());
				double nvx2  = (vy * (m - b.getMass()) + (2 * b.getMass() * b.getVY())) / (m + b.getMass());
				double nvy2 = (b.getVY() * (b.getMass() - m) + (2 * m * vy)) / (m + b.getMass());


				vx = nvx1;
				vy = nvy1;
				b.setVX(nvx2);
				b.setVY(nvy2);
				px += vx;
				py += vy;
				b.setX(b.getX() + b.getVX());
				b.setY(b.getY() + b.getVY());
			}

		}

		public boolean collides(int x, int y) {
			return Math.sqrt(Math.pow(px - x, 2) + Math.pow(py - y, 2)) <= radius;
		}

		public boolean collides(Ball b) {
			return Math.sqrt(Math.pow(px - b.getX(), 2) + Math.pow(py - b.getY(), 2)) < radius + b.getRadius();
		}

		public void paint(Graphics g) {
			g.setColor(color);
			g.fillOval((int) (px - radius), (int) (py - radius), (int) (2 * radius), (int) (2 * radius));
		}

		public void castShadow(Graphics g, Ball b) {

		}

	}

	// special ball that also acts as a light source
	private class Spirit extends Ball {

		public Spirit(int x, int y, int vx, int vy, int r) {
			super(x, y, vx, vy, r);
			color = Color.RED;
		}

		public void paint(Graphics g) {
			g.setColor(new Color(238, 238, 0, 127));
			g.fillOval((int) (px - radius), (int) (py - radius), (int) (2 * radius), (int) (2 * radius));
			//			super.paint(g);
		}

		// given a light ball and a normal ball,
		// calculates the position of a trapezoid marked by the intersections of
		// a line passing through the center of the normal ball and
		// perpendicular to the line connecting the center of both balls and
		// two points equidistant to and located on a lone perpendicular to the
		// second of the aforementioned lines
		public void castShadow(Graphics g, Ball b) {
			int CONST_MULT = 30;
			double dist = Math.sqrt(Math.pow(px - b.getX(), 2) + Math.pow(py - b.getY(), 2));
			double x1 = b.getPosition()[0];
			double y1 = b.getPosition()[1];
			double x2 = CONST_MULT * (x1 - px);
			double y2 = CONST_MULT * (y1 - py);
			double dx = x1 - x2;
			double dy = y1 - y2;
			double N = 2 * b.getRadius() / CONST_MULT;
			double N2 = 2 * b.getRadius();

			dx /= dist;
			dy /= dist;
			double x3 = x1 + (N/2)*dy;
			double y3 = y1 - (N/2)*dx;
			double x4 = x1 - (N/2)*dy;
			double y4 = y1 + (N/2)*dx;
			double x5 = x2 + (-N2/2)*dy;
			double y5 = y2 - (-N2/2)*dx;
			double x6 = x2 - (-N2/2)*dy;
			double y6 = y2 + (-N2/2)*dx;

			g.setColor(new Color(64, 64, 64, 64));
			g.fillPolygon(new int[] {(int) x3, (int) x4, (int) x5, (int) x6}, 
					new int[] {(int) y3, (int) y4, (int) y5, (int) y6}, 4);

		}

	}

}
