package applications;

import java.awt.Color;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class WebBrowser extends main.WindowedApplication {

	private static final long serialVersionUID = 1L;
	private JTextField url;
	private JButton go_url;
	private JLabel failed_webpage = null;
	
	public WebBrowser() {
		this.setBackground(Color.WHITE);
		width = 600; height = 580;
		this.setLayout(null);
		JLabel search =  new JLabel("Search: ");
		search.setBounds(5, 5, 50, 20);
		this.add(search);
		url = new JTextField(50);
		url.setEditable(true);
		url.setBounds(60, 5, 470, 20);
		//url.setText("www.gettheballgame.com");
		this.add(url);
		go_url = new JButton("Go!");
		go_url.setBounds(540, 5, 50, 20);
		//go_url.setSize(35, 20);
		go_url.setFont(new Font("Dialog", Font.PLAIN, 10));
		this.add(go_url);
		
		go_url.addActionListener(new ActionListener() {
			public void setup_button(JButton b, String text, Rectangle bounds, boolean isFake) {
				b.setText(text);
				b.setBorderPainted(false);
				b.setOpaque(false);
				b.setBackground(Color.WHITE);
				b.setHorizontalAlignment(SwingConstants.LEFT);
				if (isFake) {
					b.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							game_over();
						}
					});
				} else {
					//TODO: success: jump to download page!
				}
				b.setBounds(bounds);
				b.setForeground(Color.BLUE);
			}
			
			public void actionPerformed(ActionEvent e) {
				String s = url.getText();
				if (s.toLowerCase().contains("bouncing ball") || s.toLowerCase().contains("ball game")) { //make comparison better but also im lazy
					//show webpage with download
					System.out.println("sice");
					if (failed_webpage != null) {
						remove(failed_webpage);
						failed_webpage.setText(""); //because remove doesnt work :(
						setBackground(Color.WHITE);
					}
					//copy pasted links LOL
					JButton bad_link1 = new JButton();
					setup_button(bad_link1, "100% FREE BALL GAMES, DOWNLOAD TODAY!", new Rectangle(5, 40, 500, 20), true);
					JLabel bad_link1_url = new JLabel("www.reallytrulyfreeballgames.com");
					bad_link1_url.setBounds(25, 60, 500, 15);
					bad_link1_url.setFont(new Font("Dialog", Font.PLAIN, 10));
					add(bad_link1_url);
					JLabel bad_link1_desc = new JLabel("<html>Click here to download the world's HIGHEST QUALITY BALL GAMES! Don't delay, download today!</html>");
					bad_link1_desc.setBounds(25, 75, 500, 30);
					bad_link1_desc.setFont(new Font("Dialog", Font.PLAIN, 12));
					add(bad_link1_desc);
					add(bad_link1);
					
					JButton bad_link2 = new JButton();
					setup_button(bad_link2, "полностью законные игры в мяч", new Rectangle(5, 120, 500, 20), true); // means "totally legit ball games"
					JLabel bad_link2_url = new JLabel("t0t411yl3gitb411g4m3s.ru");
					bad_link2_url.setBounds(25, 140, 500, 15);
					bad_link2_url.setFont(new Font("Dialog", Font.PLAIN, 10));
					add(bad_link2_url);
					JLabel bad_link2_desc = new JLabel("<html>Это действительно законные игры с мячом. Не даст вам вируса. Надеюсь, этот переводчик правильный.</html>");
					//means "These are really legit ball games. Won't give you a virus. Hopefully this translator is correct."
					bad_link2_desc.setBounds(25, 155, 500, 30);
					bad_link2_desc.setFont(new Font("Dialog", Font.PLAIN, 12));
					add(bad_link2_desc);
					add(bad_link2);
					
					JButton good_link = new JButton();
					setup_button(good_link, "Ball Simulator 2000 | Downloads", new Rectangle(5, 200, 500, 20), false);
					JLabel good_link_url = new JLabel("www.collisionslab.org");
					good_link_url.setBounds(25, 220, 500, 15);
					good_link_url.setFont(new Font("Dialog", Font.PLAIN, 10));
					add(good_link_url);
					JLabel good_link_desc = new JLabel("<html>This is a demontration of the Ball Simulator 2000, a novel game which utilizes cutting-edge technology to accurately simulate bouncy balls.</html>");
					good_link_desc.setBounds(25, 235, 500, 30);
					good_link_desc.setFont(new Font("Dialog", Font.PLAIN, 12));
					add(good_link_desc);
					add(good_link);
					
					JButton bad_link3 = new JButton();
					setup_button(bad_link3, "Get a free ball game today. Click here!", new Rectangle(5, 280, 500, 20), true);
					JLabel bad_link3_url = new JLabel("www.5inzo.net/kd88k4mmdjk.swf");
					bad_link3_url.setBounds(25, 300, 500, 15);
					bad_link3_url.setFont(new Font("Dialog", Font.PLAIN, 10));
					add(bad_link3_url);
					JLabel bad_link3_desc = new JLabel("<html>This website's robots.txt is missing, so no description is available. If this is an error, please contact the system administrator of this website.</html>");
					bad_link3_desc.setBounds(25, 315, 500, 30);
					bad_link3_desc.setFont(new Font("Dialog", Font.PLAIN, 12));
					add(bad_link3_desc);
					add(bad_link3);
					
					JButton bad_link4 = new JButton();
					setup_button(bad_link4, "Free virus-free downloadable ball games - play now at deetsicer.com - NO LIMITS", new Rectangle(5, 360, 500, 20), true);
					JLabel bad_link4_url = new JLabel("deetsicer.com/ball-games/");
					bad_link4_url.setBounds(25, 380, 500, 15);
					bad_link4_url.setFont(new Font("Dialog", Font.PLAIN, 10));
					add(bad_link4_url);
					JLabel bad_link4_desc = new JLabel("<html>How many deets would a deet sicer sice if a deet sicer could sice deets? <br/>The answer: none - they keep the deets to themself.</html>");
					bad_link4_desc.setBounds(25, 395, 500, 30);
					bad_link4_desc.setFont(new Font("Dialog", Font.PLAIN, 12));
					add(bad_link4_desc);
					add(bad_link4);
					
					JButton bad_link5 = new JButton();
					setup_button(bad_link5, "<script>alert(\"free ball games! go to bit.ly/2Sh4Mc7\")</script>", new Rectangle(5, 440, 500, 20), true);
					JLabel bad_link5_url = new JLabel("bit.ly/2Sh4Mc7");
					bad_link5_url.setBounds(25, 460, 500, 15);
					bad_link5_url.setFont(new Font("Dialog", Font.PLAIN, 10));
					add(bad_link5_url);
					JLabel bad_link5_desc = new JLabel("<html>Free ball games. Free ball games online. Free bouncing ball games. Download ball games now. Get ball games for free.</html>");
					bad_link5_desc.setBounds(25, 475, 500, 30);
					bad_link5_desc.setFont(new Font("Dialog", Font.PLAIN, 12));
					add(bad_link5_desc);
					add(bad_link5);
				} else {
					//show error page
					System.out.println("unsice");
					setBackground(Color.RED);
					failed_webpage = new JLabel("<html>No results!</html>");
					failed_webpage.setBounds(100, 100, 400, 400);
					failed_webpage.setFont(new Font("Dialog", Font.PLAIN, 50));
					add(failed_webpage);
				}
			}
		});
	}
	
	public void game_over() {
		//TODO: implement game over func
	}

}
