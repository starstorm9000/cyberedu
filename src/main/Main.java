package main;

import javax.swing.JFrame;

public class Main {

	private final double TIME_BETWEEN_UPDATES = 1000000000 / 40; // 40 game updates per second
	private final double TIME_BETWEEN_REDRAW = 1000000000 / 60; // fps capped at 60
	
	private DesktopEnvironment display;
	private JFrame frame;
	
	public Main() {
		display = new DesktopEnvironment();
		frame = new JFrame("Download Simulator");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setContentPane(display);
		frame.pack();
		frame.setVisible(true);
		frame.setResizable(true);
		runMainLoop();
	}
	
	// simple game engine, should work fine for anything not CPU intensive
	// if you make something heavy, I can write a better one
	private void gameLoop() {
		double lastUpdate = System.nanoTime();
		double lastRepaint = System.nanoTime();
		
		do {
			double now = System.nanoTime();
			
			if (now - lastUpdate > TIME_BETWEEN_UPDATES) {
				update();
				lastUpdate = now;
			}
			
			if (now - lastRepaint > TIME_BETWEEN_REDRAW) {
				repaint();
				lastRepaint = now;
			}
			
		} while (true);
	}
	
	private void update() {
		display.update();
	}
	
	private void repaint() {
		display.repaint();
	}
	
	public void runMainLoop() {
		new Thread() {
			public void run() {
				gameLoop();
			}
		}.start();
	}
	
	public static void main(String[] args) {
		@SuppressWarnings("unused")
		Main desktop = new Main();
	}
	
}
