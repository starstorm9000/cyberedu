package main;
// you will write classes that extend this class
// most likely, you will be overriding the paintComponent and update
// methods of your class, as well as writing any methods any interfaces
// you choose to use implement require

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import javax.swing.JPanel;

public class WindowedApplication extends JPanel {
	
	private static final long serialVersionUID = 1L;
	private int posX, posY;
	protected int width, height;
	public final int TOP_FRAME_HEIGHT = 20, BORDER = 5;
	
	public WindowedApplication() {
		super();
		posX = posY = 0;
		width = height = 40;
		setBackground(Color.GREEN);
	}
	
	public void setLocation(int x, int y) {
		posX = x;
		posY = y;
	}
	
	public int getWidth() {
		return width;
	}
	
	public int getHeight() {
		return height;
	}
	
	public void update() {
	}

	public boolean overlapsWindowFrame(int x, int y) {
		return x >= posX - BORDER && x <= posX + width + BORDER &&
				y >= posY - TOP_FRAME_HEIGHT && y <= posY + height + BORDER;
	}
	
	public boolean overlapsExitButton(int x, int y) {
		return x >= posX + width - TOP_FRAME_HEIGHT + BORDER && x <= posX + width &&
				y >= posY - TOP_FRAME_HEIGHT && y <= posY - BORDER;
	}

	// draws the window frame
	public void drawFrame(Graphics g) {
		setBounds(posX, posY, width, height);
		setPreferredSize(new Dimension(width, height));
		posX = getLocation().x;
		posY = getLocation().y;
		g.setColor(Color.GRAY);
		g.fillRect(posX - BORDER, posY - TOP_FRAME_HEIGHT - BORDER, width + 2 * BORDER, height + TOP_FRAME_HEIGHT + 2 * BORDER);
		g.setColor(new Color(255, 68, 68));
		g.fillRect(posX + width - TOP_FRAME_HEIGHT + BORDER, posY - TOP_FRAME_HEIGHT, TOP_FRAME_HEIGHT - BORDER, TOP_FRAME_HEIGHT - BORDER);
	}

	public void setSize(int x, int y) {
		width = x;
		height = y;
	}

	public void setPos(int x, int y) {
		posX = x;
		posY = y;
	}
}
