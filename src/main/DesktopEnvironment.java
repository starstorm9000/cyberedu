package main;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import javax.swing.JPanel;

import applications.BouncingBall;
import applications.WebBrowser;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;

public class DesktopEnvironment extends JPanel implements MouseListener, MouseMotionListener {

	private static final long serialVersionUID = 1L;

	public enum States {
		DEFAULT, DRAGGING_WINDOW
	}
	private States state = States.DEFAULT;
	private Component draggedWindow;
	private int xDragOffset, yDragOffset;

	public DesktopEnvironment() {
		setLayout(null);
		addMouseListener(this);
		addMouseMotionListener(this);
		setPreferredSize(new Dimension(600, 600));
		setBackground(new Color(0, 150, 200));
		
		
		WebBrowser browser = new WebBrowser();
		browser.setPos(0, 600 - browser.getHeight());
		
		// for testing purposes, this is how you will make your application
		// appear on the desktop
		// I intend to add launcher icons in the future
		BouncingBall ball = new BouncingBall();
		// setting position and size can (should) be done within your
		// application; I include these here as a reminder to do them
		ball.setPos(50, 30);
		ball.setSize(200, 200);
		// since all applications ultimately extend JPanel, you can tell the
		// DesktopEnvironment to draw your application by adding it as a
		// component
		//this.add(ball);
		this.add(browser);
	}

	// responsible for updating all of the running programs
	public void update() {
		for (Component program : getComponents()) {
			if (program instanceof WindowedApplication) {
				((WindowedApplication) program).update();
			}
		}
	}

	// draws the window frame around each program
	// the programs' paintComponents are run when super.paintComponent() called
	@Override 
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		for (Component program : getComponents()) {
			if (program instanceof WindowedApplication) {
				((WindowedApplication) program).drawFrame(g);
			}
		}
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		if (state == States.DRAGGING_WINDOW &&
				e.getX() >= 0 && e.getX() <= getWidth() &&
				e.getY() >= 0 && e.getY() <= getHeight()) {
			draggedWindow.setLocation(e.getX() - xDragOffset, e.getY() - yDragOffset);
		}
	}

	@Override
	public void mouseMoved(MouseEvent e) {}

	@Override
	public void mouseClicked(MouseEvent e) {}

	@Override
	public void mouseEntered(MouseEvent e) {}

	@Override
	public void mouseExited(MouseEvent e) {}

	@Override
	public void mousePressed(MouseEvent e) {
		for (Component program : getComponents()) {
			if (program instanceof WindowedApplication) {
				if (((WindowedApplication) program).overlapsWindowFrame(e.getX(), e.getY())) {
					state = States.DRAGGING_WINDOW;
					draggedWindow = program;
					xDragOffset = e.getX() - draggedWindow.getX();
					yDragOffset = e.getY() - draggedWindow.getY();
				}
			}
		}
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		for (Component program : getComponents()) {
			if (program instanceof WindowedApplication) {
				if (((WindowedApplication) program).overlapsExitButton(e.getX(), e.getY())) {
					remove(program);
				}
			}
		}
		state = States.DEFAULT;
	}

}
